import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  nomcomplet='';
  nom = new FormControl('');
  prenom = new FormControl('');

  constructor() { }

  ngOnInit(): void { 
  }

  termine(){
    this.nomcomplet =this.prenom.value + " " + this.nom.value;
    this.prenom.setValue('');//on efface
    this.nom.setValue('');//on efface
  }
}
